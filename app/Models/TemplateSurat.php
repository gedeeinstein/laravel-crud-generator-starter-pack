<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TemplateSurat
 * @package App\Models
 * @version November 6, 2020, 7:28 am UTC
 *
 * @property integer $id
 * @property string $name
 */
class TemplateSurat extends Model
{
    use SoftDeletes;

    public $table = 'template_surats';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'interger',
        'name' => 'string required'
    ];

    
}

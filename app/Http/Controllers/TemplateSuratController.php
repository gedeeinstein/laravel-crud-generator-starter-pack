<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTemplateSuratRequest;
use App\Http\Requests\UpdateTemplateSuratRequest;
use App\Repositories\TemplateSuratRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TemplateSuratController extends AppBaseController
{
    /** @var  TemplateSuratRepository */
    private $templateSuratRepository;

    public function __construct(TemplateSuratRepository $templateSuratRepo)
    {
        $this->templateSuratRepository = $templateSuratRepo;
    }

    /**
     * Display a listing of the TemplateSurat.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $templateSurats = $this->templateSuratRepository->all();

        return view('template_surats.index')
            ->with('templateSurats', $templateSurats);
    }

    /**
     * Show the form for creating a new TemplateSurat.
     *
     * @return Response
     */
    public function create()
    {
        return view('template_surats.create');
    }

    /**
     * Store a newly created TemplateSurat in storage.
     *
     * @param CreateTemplateSuratRequest $request
     *
     * @return Response
     */
    public function store(CreateTemplateSuratRequest $request)
    {
        $input = $request->all();

        $templateSurat = $this->templateSuratRepository->create($input);

        Flash::success('Template Surat saved successfully.');

        return redirect(route('templateSurats.index'));
    }

    /**
     * Display the specified TemplateSurat.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $templateSurat = $this->templateSuratRepository->find($id);

        if (empty($templateSurat)) {
            Flash::error('Template Surat not found');

            return redirect(route('templateSurats.index'));
        }

        return view('template_surats.show')->with('templateSurat', $templateSurat);
    }

    /**
     * Show the form for editing the specified TemplateSurat.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $templateSurat = $this->templateSuratRepository->find($id);

        if (empty($templateSurat)) {
            Flash::error('Template Surat not found');

            return redirect(route('templateSurats.index'));
        }

        return view('template_surats.edit')->with('templateSurat', $templateSurat);
    }

    /**
     * Update the specified TemplateSurat in storage.
     *
     * @param int $id
     * @param UpdateTemplateSuratRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTemplateSuratRequest $request)
    {
        $templateSurat = $this->templateSuratRepository->find($id);

        if (empty($templateSurat)) {
            Flash::error('Template Surat not found');

            return redirect(route('templateSurats.index'));
        }

        $templateSurat = $this->templateSuratRepository->update($request->all(), $id);

        Flash::success('Template Surat updated successfully.');

        return redirect(route('templateSurats.index'));
    }

    /**
     * Remove the specified TemplateSurat from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $templateSurat = $this->templateSuratRepository->find($id);

        if (empty($templateSurat)) {
            Flash::error('Template Surat not found');

            return redirect(route('templateSurats.index'));
        }

        $this->templateSuratRepository->delete($id);

        Flash::success('Template Surat deleted successfully.');

        return redirect(route('templateSurats.index'));
    }
}
